from functools import reduce 
def exercise_one():
    for i in range(1,101): 
        if (i % 3 == 0 and i % 5 == 0): 
            print("ThreeFive") 
        elif (i % 3 == 0): 
            print("Three") 
        elif (i % 5 == 0):  
            print("Five") 
        else:  
            print(i)
    pass


def is_colorful(n): 
    digits = [int(d) for d in str(n)] 
    seen = set() 

    # Iterate over all possible substrings of size 2 or more and check if product of the numbers is already present in "seen"  
    for i in range(1, len(digits)+1): 
        for j in range (i):  
            prod = reduce((lambda x, y: x * y), digits[j:i])  

            if prod in seen: return False  # If a product appears twice then number can't be colorful.

            seen.add(prod)  

    return True
# Driver Code     

def calculate(lst):
    if not isinstance(lst, list):
        return False
    result = 0
    for item in lst:
        if isinstance(item, str):
            try:
                result += int(item)
            except ValueError:
                pass
        
    return result

def anagrams(word, words):
    word_sorted = ''.join(sorted(word))
    return [w for w in words if ''.join(sorted(w)) == word_sorted]



