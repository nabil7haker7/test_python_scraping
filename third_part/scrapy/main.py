import scrapy

class IcedTeasSpider(scrapy.Spider):
    name = "iced_teas_spider"
    start_urls = [
        'https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas'
    ]

    def parse(self, response):
        breadcrumb = response.xpath('//nav[@class="breadcrumb"]//a/text()').getall()
        product_names = response.xpath('//h2[@class="product-card__title"]/text()').getall()
        
        for product_name in product_names:
            item = {
                'breadcrumb': breadcrumb,
                'product_name': product_name.strip(),
            }
            yield item
