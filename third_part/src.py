import requests
import json
import gzip
import logging
import csv
import math
def http_request(url, params, headers):
    response = requests.post(url, data=params, headers=headers)
    return response.json()


params = {'isadmin': 1}
headers = {'User-Agent': 'My custom user agent'}

response_body = http_request('https://httpbin.org/anything', params, headers)
#print(response_body)




class ProductImporter:
    def __init__(self, file_path):
        self.file_path = file_path
        self.products = []

    def import_data(self):
        with gzip.open(self.file_path, "r") as f:
            data = json.load(f)
            for x in data["Bundles"]:

                    try:
                        
                        
                        if(len(x['Products'])):
                            product=x['Products'][0]
                            name = x["Name"][:30]
                            price = product["Price"]
                            availability = product["IsInStock"]
                            
                            if availability:
                                self.products.append({"Name": name, "Price": price})
                                print(f"You can buy {name} at our store at {price}")
                            else:
                                logging.warning(f"Product with ID {product['Stockcode']} and name {name} is unavailable")
                        else:
                         pass
                    except KeyError:
                        pass

    def save_to_csv(self, file_path):
        with open(file_path, "w") as f:
            writer = csv.DictWriter(f, fieldnames=["Name", "Price"])
            writer.writeheader()
            writer.writerows(self.products)

if __name__ == "__main__":
    importer = ProductImporter("data\\data.json.gz")
    importer.import_data()
    importer.save_to_csv("available_products.csv")