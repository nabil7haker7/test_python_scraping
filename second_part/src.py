import random
import unittest
def random_gen():
    while True:
        rand_num = random.randint(10, 20)
        if rand_num == 15:
            break
        yield rand_num


def decorator_to_str(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return str(result)
    return wrapper


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']


def ignore_exception(exception):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return None
        return wrapper
    return decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception

# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList(type):
    def __init__(cls, name, bases, attrs):
        attrs['append'] = lambda self, value: self.append(value)
        attrs['extend'] = lambda self, values: self.extend(values)
        attrs['insert'] = lambda self, index, value: self.insert(index, value)
        super().__init__(name, (list, ) + bases, attrs)



class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

class MetaCheckProcess(type):
    def __init__(cls, name, bases, attrs):
        if 'process' not in attrs:
            raise TypeError("Class must have a 'process' method")
        if not callable(attrs['process']) or len(signature(attrs['process']).parameters) != 3:
            raise TypeError("'process' method must be a function taking 3 arguments")
        super().__init__(name, bases, attrs)




class TestCacheDecorator(unittest.TestCase):
    def test_cache_decorator(self):
        cache_decorator = CacheDecorator()

        @cache_decorator
        def square(x):
            return x * x

        self.assertEqual(square(2), 4)
        self.assertEqual(cache_decorator.cache, {2: 4})
        self.assertEqual(square(2), 4)
        self.assertEqual(cache_decorator.cache, {2: 4})
        self.assertEqual(square(3), 9)
        self.assertEqual(cache_decorator.cache, {2: 4, 3: 9})

    def test_cache_decorator_with_args_and_kwargs(self):
        cache_decorator = CacheDecorator()

        @cache_decorator
        def sum_of_squares(x, y, z=0):
            return x * x + y * y + z * z

        self.assertEqual(sum_of_squares(2, 3), 13)
        self.assertEqual(cache_decorator.cache, {(2, 3): 13})
        self.assertEqual(sum_of_squares(2, 3), 13)
        self.assertEqual(cache_decorator.cache, {(2, 3): 13})
        self.assertEqual(sum_of_squares(2, 3, 4), 29)
        self.assertEqual(cache_decorator.cache, {(2, 3): 13, (2, 3, 4): 29})

if __name__ == '__main__':
    unittest.main()